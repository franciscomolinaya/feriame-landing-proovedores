import * as React from "react"

function Dot(props) {
  return (
    <svg
      width={31}
      height={31}
      viewBox="0 0 31 31"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M24.816 3.615c6.375 5.42 7.254 15.075 1.832 21.536-5.35 6.462-14.875 7.353-21.25 1.857-6.375-5.421-7.254-15.076-1.832-21.537 5.35-6.46 14.875-7.352 21.25-1.857z"
        fill={props.fill ? props.fill : "#00C3AC"}
      />
    </svg>
  )
}

export default Dot
