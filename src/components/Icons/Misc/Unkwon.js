import * as React from "react";

const Unknown = (props) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} viewBox="0 0 16 16" {...props}>
      <path
        d="M8 2a4 4 0 00-4 4h1a3 3 0 013-3 3 3 0 013 3 3 3 0 01-3 3H7v3h1v-2a4 4 0 004-4 4 4 0 00-4-4M7 13v1h1v-1H7"
        fill="#4d4d4d"
      />
    </svg>
  );
};

export default Unknown;
