import * as React from "react";

function Clip(props) {
  return (
    <svg
      width={60}
      height={122}
      viewBox="0 0 60 122"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M40.24 114.127c3.371 3.416 8.94 3.416 12.31 0a8.884 8.884 0 002.566-6.238c0-2.377-.88-4.605-2.565-6.238L-40.802 6.962c-1.612-1.633-3.81-2.599-6.155-2.599s-4.543.891-6.155 2.6a8.883 8.883 0 00-2.565 6.238c0 2.376.88 4.604 2.565 6.238l93.353 94.688zm6.156 6.981c-3.37 0-6.668-1.262-9.233-3.862L-56.19 22.558C-58.68 20.033-60 16.765-60 13.201c0-3.565 1.392-6.833 3.81-9.358 5.056-5.124 13.336-5.124 18.466 0l93.352 94.614c5.056 5.124 5.056 13.516 0 18.715-2.564 2.599-5.862 3.936-9.232 3.936z"
        fill="#F38EB0"
      />
    </svg>
  );
}

export default Clip;
