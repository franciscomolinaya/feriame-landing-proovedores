import * as React from "react"

function STWaveDiv2(props) {
  return (
    <svg
      width={330}
      height={257}
      viewBox="0 0 330 257"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        opacity={0.3}
        d="M330 0a9.71 9.71 0 01-.37 1.57c-11.482 43.151-72.686 51.744-104.167 84.73-53.241 55.809-36.389 135.734-127.407 162.9C57.5 261.304 24.907 258.624 0 245.966V0h330z"
        fill="#88D7CD"
      />
    </svg>
  )
}

export default STWaveDiv2
