import * as React from "react"

function STWaveDiv(props) {
  return (
    <svg
      width={383}
      height={387}
      viewBox="0 0 383 387"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        opacity={0.3}
        d="M383 0v673H0c12.271-7.1 24.866-12.909 34.77-16.997 43.595-17.965 92.358-30.552 123.791-64.546 39.398-42.493 39.936-105.425 35.953-162.011-3.983-56.585-8.934-117.904 23.251-165.668 25.835-38.297 72.122-61.749 94.189-102.09 15.501-28.293 17.008-61.426 24.651-92.624C343.494 41.74 357.811 13.124 383 0z"
        fill="#88D7CD"
      />
    </svg>
  )
}

export default STWaveDiv
