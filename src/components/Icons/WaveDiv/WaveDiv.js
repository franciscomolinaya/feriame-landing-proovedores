import * as React from "react"

function WaveDiv(props) {
  return (
    <svg
      width={340}
      height={171}
      viewBox="0 0 340 171"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M354.952 168.014a16.77 16.77 0 01-1.759.407c-48.669 10.3-73.657-26.677-116.756-33.844-72.913-12.109-153.163 32.22-205.556-16.801C7.536 95.934-10.825 44.026 7.56-.77L351.848-70.35l3.104 238.363z"
        fill="#F2F2F2"
      />
    </svg>
  )
}

export default WaveDiv
