import React from "react";
import FeriameFull from "./components/Icons/FeriameFull/FeriameFull";
import "./App.css";
import WaveDiv from "./components/Icons/WaveDiv/WaveDiv";
import STWaveDiv from "./components/Icons/STWaveDiv/STWaveDiv";
import STWaveDiv2 from "./components/Icons/STWaveDiv2/STWaveDiv2";
import DotsGrid from "./components/Icons/DotsGrid/DotsGrid";
import YellowBlob from "./components/Icons/Misc/YellowBlob";
import BlobsGrid from "./components/Icons/Misc/BlobsGrid";
import Dot from "./components/Icons/Misc/Dot";
import SemiCircle from "./components/Icons/Misc/SemiCircle";
import OutlinedSemiCircle from "./components/Icons/Misc/OutlinedSemiCircle";
import OutlinedMiniLine from "./components/Icons/Misc/OutlinedMiniLine";
import MiniWave from "./components/Icons/Misc/MiniWave";

function App() {
  const Hero = () => {
    return (
      <div className={"content-wrapper"}>
        <FeriameFull width={350} height={180} />
        <h3 className={"subtitle-logo"}>TU MERCADO DE CERCANÍA</h3>
        <h1 className={"title"}>
          Una nueva forma de comprar y vender productos regionales
        </h1>
        <h2 className={"subtitle"}>MUY PRONTO VAS A DESCUBRIRLA</h2>
      </div>
    );
  };

  const Contactanos = () => {
    return (
      <div className={"contactanos-wrapper"}>
        <WaveDiv />
        <div className={"contactanos-content-wrapper"}>
          <h3 className={"queres-vender"}>¿Querés vender en feriame?</h3>
          <button
            type="button"
            className={"button"}
            onClick={() => window.open("https://feriame.com/contacto", "_self")}
          >
            Contactanos
          </button>
        </div>
      </div>
    );
  };

  const Blobs = () => {
    return (
      <>
        <div className={"blob-1"}>
          <STWaveDiv />
        </div>
        <div className={"blob-2"}>
          <STWaveDiv2 />
        </div>
        <div className={"blob-3"}>
          <DotsGrid />
        </div>
        <div className={"blob-4"}>
          <DotsGrid />
        </div>
        <div className={"blob-5"}>
          <YellowBlob />
        </div>
        <div className={"blob-6"}>
          <BlobsGrid />
        </div>
        <div className={"blob-7"}>
          <BlobsGrid />
        </div>
        <div className={"blob-8"}>
          <Dot />
        </div>
        <div className={"blob-9"}>
          <Dot />
        </div>
        <div className={"blob-10"}>
          <Dot />
        </div>
        <div className={"blob-11"}>
          <Dot fill={"#FF745F"} />
        </div>
        <div className={"blob-12"}>
          <Dot fill={"#FF745F"} />
        </div>
        <div className={"blob-13"}>
          <Dot fill={"#FF745F"} />
        </div>
        <div className={"blob-14"}>
          <SemiCircle />
        </div>
        <div className={"blob-15"}>
          <SemiCircle />
        </div>
        <div className={"blob-16"}>
          <SemiCircle />
        </div>
        <div className={"blob-17"}>
          <OutlinedSemiCircle />
        </div>
        <div className={"blob-18"}>
          <OutlinedMiniLine />
        </div>
        <div className={"blob-19"}>
          <MiniWave />
        </div>
      </>
    );
  };

  return (
    <div className="App">
      <div className={"main-wrapper"}>
        <Blobs />

        <Hero />
        <Contactanos />
      </div>
    </div>
  );
}

export default App;
